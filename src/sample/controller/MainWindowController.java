package sample.controller;

import javafx.event.ActionEvent;


public class MainWindowController {

    public void pressButtonExit(ActionEvent exit) {
        System.exit(0);
    }

    public void returnText(ActionEvent text) {
        System.out.println("Hello Darkness my old friend");
    }
}

